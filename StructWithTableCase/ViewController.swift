//
//  ViewController.swift
//  StructWithTableCase
//
//  Created by Nheng Vanchhay on 29/6/21.
//

import UIKit

struct multiTypeStruct <T> {
    var title                   : String
    var value                   : T?
}

//PRIVACY
struct privacy {
    var title   : String
    var object  : [object]

    struct object {
        let title  : String?
        let value  : String?
    }
}

//BizPoint
struct bizpoint {
    var title   : String
//    var subTitle: String
    let object  : [object]
    
    struct object {
        let title     : String?
        let value  : String?
    }
}



class ViewController: UIViewController {

    var obj: multiTypeStruct<Any>?
    
    var ss: privacy?
    
    var array = [multiTypeStruct<Any>?]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        multi()
    }
    
    func multi() {
        
        array = [
            multiTypeStruct(title: "privacy",
                            value: privacy(title: "privacy", object: [privacy.object(title: "object privacy", value: "object privacy")])),
            multiTypeStruct(title: "bizpoint",
                            value: privacy(title: "bizpoint", object: [privacy.object(title: "object bizpoint", value: "object bizpoint")]))
        ]
        
   
        for i in array {
            ss = i?.value as! privacy
            print(ss?.title)
            print(ss?.object[0].title)
        }
    }
}




